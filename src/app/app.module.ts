import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http"
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InsertProductComponent } from './insert-product/insert-product.component';
import {FormsModule} from '@angular/forms';
import { MenuComponent } from './menu/menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { CartComponent } from './cart/cart.component';
import { ErrorComponent } from './error/error.component';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    InsertProductComponent,
    MenuComponent,
    
    FooterComponent,
    
    HomeComponent,
    
    LoginComponent,
    
    LogoutComponent,
    
    CartComponent,
    
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
