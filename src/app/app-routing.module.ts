import { ErrorComponent } from './error/error.component';
import { InsertProductComponent } from './insert-product/insert-product.component';
import { CartComponent } from './cart/cart.component';
import { LogoutComponent } from './logout/logout.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:'',component:HomeComponent,pathMatch:'full'},
  {path:'login',component:LoginComponent,pathMatch:"full"},
  {path:'logout',component:LogoutComponent,pathMatch:'full'},
  {path:'cart',component:CartComponent,pathMatch:'full'},
  {path:'admin-manageProduct',component:InsertProductComponent,pathMatch:'full'},
  {path:'**',component:ErrorComponent,pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
