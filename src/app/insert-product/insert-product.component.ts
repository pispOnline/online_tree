import { AdminProductService } from './../admin-product.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'insert-product',
  templateUrl: './insert-product.component.html',
  styleUrls: ['./insert-product.component.css']
})
export class InsertProductComponent implements OnInit {

  



  constructor(private adminProduct:AdminProductService) { }

  ngOnInit() {
  }
  Save(formProduct){
      this.adminProduct.getProductFromComponent(formProduct);
  }
}
